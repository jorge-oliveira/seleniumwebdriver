package newpackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDownBox {

	public static void main(String[] args) {

		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.gecko.driver",
				"/home/mufy/git/seleniumwebdriver/seleniumProject/src/newpackage/drivers/geckodriver");
		WebDriver driver = new FirefoxDriver();

		String baseUrl = "http://demo.guru99.com/test/newtours/register.php";

		driver.get(baseUrl);

		Select drpCountry = new Select(driver.findElement(By.name("country")));
		drpCountry.selectByVisibleText("PORTUGAL");
 
		// Selecting Items in a Multiple SELECT elements
		driver.get("http://jsbin.com/osebed/2");
		Select fruits = new Select(driver.findElement(By.id("fruits")));
		fruits.selectByVisibleText("Banana");
		fruits.selectByIndex(1);
	
		driver.quit();
	}

}
