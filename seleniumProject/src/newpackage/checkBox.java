package newpackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class checkBox {

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver",
				"/home/mufy/git/seleniumwebdriver/seleniumProject/src/newpackage/drivers/geckodriver");
		WebDriver driver = new FirefoxDriver();
		String alertMessage = "";

		driver.get("http://demo.guru99.com/test/radio.html");

		// get the WebElement corresponding to the Email Address(TextField)
		WebElement radio1 = driver.findElement(By.id("vfb-7-1"));

		// get the WebElement corresponding to the Email Address(TextField)
		WebElement radio2 = driver.findElement(By.id("vfb-7-2"));

		// click on the button
		radio1.click();

		radio2.click();

		driver.quit();

	}
}
