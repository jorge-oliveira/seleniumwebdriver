package newpackage;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class AppTest {

	protected WebDriver driver;

	@Test
	public void guru99tutorials() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				"//home/mufy/git/seleniumwebdriver/seleniumProject/src/newpackage/drivers/chromedriver");
		WebDriver driver = new ChromeDriver();

		/*
		 * In the below example we have declared an implicit wait with the time frame of
		 * 10 seconds. It means that if the element is not located on the web page
		 * within that time frame, it will throw an exception.
		 * 
		 */
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		String eTitle = "Demo Guru99 Page";
		String aTitle = "";

		// launch Chrome and redirect it to the Base URL
		driver.get("http://demo.guru99.com/test/guru99home/");

		// Maximizes the browser window
		driver.manage().window().maximize();

		// get the actual value of the title
		aTitle = driver.getTitle();

		// compare the actual title with the expected title
		if (aTitle.equals(eTitle)) {
			System.out.println("Test Passed");
		} else {
			System.out.println("Test Failed");
		}

		// close browser
		driver.close();
	}
}