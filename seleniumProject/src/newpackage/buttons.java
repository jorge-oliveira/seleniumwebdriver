package newpackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class buttons {

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver",
				"/home/mufy/git/seleniumwebdriver/seleniumProject/src/newpackage/drivers/geckodriver");
		WebDriver driver = new FirefoxDriver();
		String alertMessage = "";

		driver.get("http://demo.guru99.com/test/login.html");

		// get the WebElement corresponding to the Email Address(TextField)
		WebElement email = driver.findElement(By.id("email"));

		// get the WebElement corresponding to the Email Address(TextField)
		WebElement password = driver.findElement(By.name("passwd"));

		email.sendKeys("test@email.com");

		password.sendKeys("pass123456");

		// get the WebElement login by id
		WebElement login = driver.findElement(By.id("SubmitLogin"));

		// click on the button
		login.click();

		/*
		 * Submit() method can be call from any form element 
		 * 
		 * login.submit();
		 * password.submit();
		 * 
		 */

		driver.quit();

	}
}
