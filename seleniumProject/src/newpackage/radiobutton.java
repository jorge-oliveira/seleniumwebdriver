package newpackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class radiobutton {

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver",
				"/home/mufy/git/seleniumwebdriver/seleniumProject/src/newpackage/drivers/geckodriver");
		WebDriver driver = new FirefoxDriver();
		String alertMessage = "";

		driver.get("http://www.facebook.com");

		WebElement chkFBPersist = driver.findElement(By.id("persist_box"));
		
		// ciclo para clicar em cada elemento
		for(int i=0; i<2; i++) {
			chkFBPersist.click();
			// informa se true ou false
			System.out.println(chkFBPersist.isSelected());
		}

		driver.quit();

	}
}
